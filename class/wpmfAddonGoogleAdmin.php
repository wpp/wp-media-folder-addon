<?php
/* Prohibit direct script loading */
defined('ABSPATH') || die('No direct script access allowed!');
require_once(WPMFAD_PLUGIN_DIR . '/class/wpmfGoogle.php');
require_once(WPMFAD_PLUGIN_DIR . '/class/wpmfHelper.php');
require_once(WPMFAD_PLUGIN_DIR . '/class/Google/autoload.php');

/**
 * Class WpmfAddonGoogle
 * This class that holds most of the admin functionality for Google drive
 */
class WpmfAddonGoogle extends WpmfAddonGoogleDrive
{

    /**
     * WpmfAddonGoogle constructor.
     */
    public function __construct()
    {
        parent::__construct();
        if (is_plugin_active('wp-media-folder/wp-media-folder.php')) {
            add_action('admin_menu', array($this, 'addMenuPage'));
            add_action('admin_enqueue_scripts', array($this, 'registerStyleScript'));
            add_action('enqueue_block_editor_assets', array($this, 'addEditorAssets'), 9999);
            add_action('enqueue_block_assets', array($this, 'addEditorStyles'), 9999);
            add_filter('media_upload_tabs', array($this, 'addUploadTab'));
            add_action('media_upload_wpmfgg', array($this, 'mediaUpload'));
        }
        add_action('wp_ajax_wpmf-get-filelist', array($this, 'getGoogleFilelist'));
        add_action('wp_ajax_wpmf-google-addfolder', array($this, 'ajaxCreateFolder'));
        add_action('wp_ajax_wpmf-google-editfolder', array($this, 'changeFilename'));
        add_action('wp_ajax_wpmf-google-deletefolder', array($this, 'delete'));
        add_action('wp_ajax_wpmfaddon_move_file', array($this, 'moveFile'));
        add_action('wp_ajax_wpmf-upload-file', array($this, 'uploadFile'));
        add_action('wp_ajax_wpmf-download-file', array($this, 'downloadFile'));
        add_action('wp_ajax_nopriv_wpmf-download-file', array($this, 'downloadFile'));
        add_action('wp_ajax_wpmf_ggimport_file', array($this, 'importFile'));
        add_action('wp_ajax_wpmf-preview-file', array($this, 'previewFile'));
        add_action('wp_ajax_nopriv_wpmf-preview-file', array($this, 'previewFile'));
        add_filter('wpmfaddon_ggsettings', array($this, 'tabGoogle'), 10, 2);
    }

    /**
     * Google drive settings html
     *
     * @param object $googleDrive  WpmfAddonGoogleDrive class
     * @param array  $googleconfig Google config options
     *
     * @return string
     */
    public function tabGoogle($googleDrive, $googleconfig)
    {
        ob_start();
        require_once 'templates/settings_google_drive.php';
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }

    /**
     * Add a tab to media menu in iframe
     *
     * @param array $tabs An array of media tabs
     *
     * @return array
     */
    public function addUploadTab($tabs)
    {
        $googleDrive = new WpmfAddonGoogleDrive();
        if ($googleDrive->checkAuth()) {
            $newtab = array('wpmfgg' => __('Insert Google Drive', 'wpmfAddon'));
            return array_merge($tabs, $newtab);
        }
        return $tabs;
    }

    /**
     * Create iframe
     *
     * @return void
     */
    public function mediaUpload()
    {
        $errors = false;
        wp_iframe(array($this, 'mediaUploadForm'), $errors);
    }

    /**
     * Load html iframe
     *
     * @return void
     */
    public function mediaUploadForm()
    {
        $googleDrive = new WpmfAddonGoogleDrive();
        if (!$googleDrive->checkAuth()) {
            $message       = __('The connection to Google Drive is not established,
             you can do that from the WP Media configuration', 'wpmfAddon');
            $link_setting  = admin_url('options-general.php?page=option-folder#cloud');
            $link_document = 'https://www.joomunited.com/documentation/wp-media-folder-cloud-addon#toc-ii-connect-google-drive';
            $open_new      = true;
            require_once(WPMFAD_PLUGIN_DIR . '/class/templates/error_message.php');
        } else {
            $this->loadStyleScript();
            $mediatype = 'google';
            require_once(WPMFAD_PLUGIN_DIR . '/class/templates/listfiles.php');
        }
    }

    /**
     * Load style and script
     *
     * @return void
     */
    public function loadStyleScript()
    {
        wp_enqueue_style('wpmf-google-icon');
        wp_enqueue_style('wpmf-css-font-material-design');
        wp_enqueue_style('wpmf-css-googlefile');
        wp_enqueue_style('wpmf-css-popup');
        wp_enqueue_script('jquery-ui-draggable');
        wp_enqueue_script('jquery-ui-droppable');
        wp_enqueue_script('wpmf-loadgooglefile');
        wp_enqueue_script('wpmf-imagesloaded');
        wp_enqueue_script('wpmf-popup');
        wp_enqueue_script('jquery-ui-dialog');
        wp_enqueue_style('wpmf-css-dialogs');
        wp_enqueue_script('jQuery.fileupload');
        wp_enqueue_script('jQuery.fileupload-process');
        wp_enqueue_style('wpmf-fileupload-jquery-ui');
        wp_enqueue_style('wpmftree');
    }

    /**
     * Load scripts and style
     *
     * @return void
     */
    public function registerStyleScript()
    {
        wp_register_style(
            'wpmf-google-icon',
            'https://fonts.googleapis.com/icon?family=Material+Icons'
        );
        wp_register_script(
            'wpmf-imagesloaded',
            plugins_url('/assets/js/imagesloaded.pkgd.min.js', dirname(__FILE__)),
            array(),
            '3.1.5',
            true
        );
        wp_register_script(
            'wpmf-popup',
            plugins_url('/assets/js/jquery.magnific-popup.min.js', dirname(__FILE__)),
            array('jquery'),
            '0.9.9',
            true
        );
        wp_register_script(
            'wpmf-loadgooglefile',
            plugins_url('/assets/js/loadgooglefile.js', dirname(__FILE__)),
            array('jquery'),
            WPMFAD_VERSION
        );
        wp_register_script(
            'jQuery.fileupload',
            plugins_url('/assets/js/fileupload/jquery.fileupload.js', dirname(__FILE__)),
            array('jquery'),
            false,
            true
        );
        wp_register_script(
            'jQuery.fileupload-process',
            plugins_url('/assets/js/fileupload/jquery.fileupload-process.js', dirname(__FILE__)),
            array('jquery'),
            false,
            true
        );
        wp_register_style(
            'wpmf-css-googlefile',
            plugins_url('/assets/css/style.css', dirname(__FILE__)),
            array(),
            WPMFAD_VERSION
        );
        wp_register_style(
            'wpmf-css-font-material-design',
            plugins_url('/assets/css/material-design-iconic-font.min.css', dirname(__FILE__)),
            array(),
            WPMFAD_VERSION
        );
        wp_register_style(
            'wpmf-css-popup',
            plugins_url('/assets/css/magnific-popup.css', dirname(__FILE__)),
            array(),
            '0.9.9'
        );
        wp_register_style(
            'wpmf-css-dialogs',
            plugins_url('/assets/css/jquery-ui-1.10.3.custom.css', dirname(__FILE__)),
            array(),
            '1.10.3'
        );
        wp_register_style(
            'wpmftree',
            plugins_url('/assets/css/jaofiletree.css', dirname(__FILE__)),
            array(),
            WPMFAD_VERSION
        );
        wp_register_style(
            'wpmf-fileupload-jquery-ui',
            plugins_url('/assets/css/jquery.fileupload-ui.css', dirname(__FILE__))
        );
        wp_localize_script('wpmf-loadgooglefile', 'wpmfaddonparams', $this->localizeScript());
    }

    /**
     * Enqueue styles and scripts for gutenberg
     *
     * @return void
     */
    public function addEditorAssets()
    {
        wp_enqueue_script(
            'wpmfgoogle_blocks',
            plugins_url('assets/blocks/wpmfgoogle/block.js', dirname(__FILE__)),
            array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-data', 'wp-editor' ),
            WPMFAD_VERSION
        );

        $params = array(
            'l18n' => array(
                'btnopen' => __('Load Google Drive media', 'wpmfAddon'),
                'google_drive' => __('Google Drive', 'wpmfAddon'),
                'edit' => __('Edit', 'wpmfAddon'),
                'remove' => __('Remove', 'wpmfAddon')
            ),
            'vars' => array('admin_google_page' => admin_url('upload.php?page=wpmf-google-drive-page&noheader=1&editor=gutenberg'))
        );

        wp_localize_script('wpmfgoogle_blocks', 'wpmfblocks', $params);
    }

    /**
     * Enqueue styles for gutenberg editor and front-end
     *
     * @return void
     */
    public function addEditorStyles()
    {
        wp_enqueue_style(
            'wpmfgoogle_blocks',
            plugins_url('assets/blocks/wpmfgoogle/style.css', dirname(__FILE__)),
            array(),
            WPMFAD_VERSION
        );
    }

    /**
     * Localize a script.
     * Works only if the script has already been added.
     *
     * @return array
     */
    public function localizeScript()
    {
        $wpmfAddon_cloud_config = get_option('_wpmfAddon_cloud_config');
        if (isset($wpmfAddon_cloud_config['googleBaseFolder'])) {
            $googleBaseFolder = $wpmfAddon_cloud_config['googleBaseFolder'];
        } else {
            $googleBaseFolder = 0;
        }
        return array(
            'googleBaseFolder'    => $googleBaseFolder,
            'newfolder'           => __('New Folder', 'wpmfAddon'),
            'addfolder'           => __('Add Folder', 'wpmfAddon'),
            'editfolder'          => __('Change Filename', 'wpmfAddon'),
            'cancelfolder'        => __('Cancel', 'wpmfAddon'),
            'promt'               => __('Please give a name to this new folder', 'wpmfAddon'),
            'save'                => __('Save', 'wpmfAddon'),
            'delete'              => __('Delete', 'wpmfAddon'),
            'deletefolder'        => __('Delete Folder', 'wpmfAddon'),
            'upload_nonce'        => wp_create_nonce('wpmf-upload-file'),
            'maxNumberOfFiles'    => __('Maximum number of files exceeded', 'wpmfAddon'),
            'acceptFileTypes'     => __('File type not allowed', 'wpmfAddon'),
            'maxFileSize'         => __('File is too large', 'wpmfAddon'),
            'minFileSize'         => __('File is too small', 'wpmfAddon'),
            'plugin_url'          => plugins_url('/assets/images/icons/', dirname(__FILE__)),
            'str_inqueue'         => __('In queue', 'wpmfAddon'),
            'str_uploading_local' => __('Uploading to Server', 'wpmfAddon'),
            'str_uploading_cloud' => __('Uploading', 'wpmfAddon'),
            'str_success'         => __('Success', 'wpmfAddon'),
            'str_error'           => __('Error', 'wpmfAddon'),
            'str_message_delete'  => __('These items will be permanently deleted and
             cannot be recovered. Are you sure?', 'wpmfAddon'),
            'maxsize'             => 104857600,
            'media_folder'        => __('Media Library', 'wpmfAddon'),
            'message_import'      => __('Files imported with success!', 'wpmfAddon'),
            'wpmf_nonce'            => wp_create_nonce('wpmf_nonce'),
            'admin_url'            => admin_url()
        );
    }

    /**
     * Add menu media page
     *
     * @return void
     */
    public function addMenuPage()
    {
        $googleDrive = new WpmfAddonGoogleDrive();
        if ($googleDrive->checkAuth()) {
            add_media_page(
                'Google Drive',
                'Google Drive',
                'upload_files',
                'wpmf-google-drive-page',
                array($this, 'showGoogleDriveFile')
            );
        }
    }

    /**
     * Google drive page
     *
     * @return void
     */
    public function showGoogleDriveFile()
    {
        $googleDrive = new WpmfAddonGoogleDrive();
        if (!$googleDrive->checkAuth()) {
            $message       = __('The connection to Google Drive is not established,
             you can do that from the WP Media configuration', 'wpmfAddon');
            $link_setting  = admin_url('options-general.php?page=option-folder#cloud');
            $link_document = 'https://www.joomunited.com/documentation/wp-media-folder-cloud-addon#toc-ii-connect-google-drive';
            $open_new      = false;
            require_once(WPMFAD_PLUGIN_DIR . '/class/templates/error_message.php');
        } else {
            // phpcs:ignore WordPress.Security.NonceVerification.NoNonceVerification -- No action, nonce is not required
            if (isset($_GET['noheader'])) {
                _wp_admin_html_begin();
                global $hook_suffix;
                do_action('admin_enqueue_scripts', $hook_suffix);
                do_action('admin_print_scripts-' . $hook_suffix);
                do_action('admin_print_scripts');
                ?>
                <style>
                    #wpfooter {
                        display: none;
                    }
                </style>
                <?php
            }
            $this->loadStyleScript();
            $mediatype = 'google';
            // phpcs:ignore WordPress.Security.NonceVerification.NoNonceVerification -- No action, nonce is not required
            if (isset($_GET['editor']) && $_GET['editor'] === 'gutenberg') {
                $editor_type = 'wpmfgutenberg';
            }

            // phpcs:ignore WordPress.Security.NonceVerification.NoNonceVerification -- No action, nonce is not required
            if (isset($_GET['idblock']) && $_GET['idblock'] !== '') {
                // phpcs:ignore WordPress.Security.NonceVerification.NoNonceVerification -- No action, nonce is not required
                $editor_idblock = $_GET['idblock'];
            } else {
                $editor_idblock = '';
            }

            // phpcs:ignore WordPress.Security.NonceVerification.NoNonceVerification -- No action, nonce is not required
            if (!empty($_GET['fileId'])) {
                // phpcs:ignore WordPress.Security.NonceVerification.NoNonceVerification -- No action, nonce is not required
                $selectedFile = $_GET['fileId'];
            } else {
                $selectedFile = '';
            }

            require_once(WPMFAD_PLUGIN_DIR . '/class/templates/listfiles.php');
        }
    }

    /**
     * Access google drive
     *
     * @return void
     */
    public function ggAuthenticated()
    {
        $google      = new WpmfAddonGoogleDrive();
        $credentials = $google->authenticate();
        $google->storeCredentials($credentials);
        //Check if WPMF folder exists and create if not
        if (!$google->folderExists(WpmfAddonHelper::getDataConfigBySeverName('google'))) {
            $folder                   = $google->createFolder('WP Media Folder - ' . get_bloginfo('name'));
            $data                     = $this->getParams();
            $data['googleBaseFolder'] = $folder->id;
            $this->setParams($data);
        }
        $this->redirect(admin_url('options-general.php?page=option-folder#cloud'));
    }

    /**
     * Get google config
     *
     * @return mixed
     */
    public function getParams()
    {
        return WpmfAddonHelper::getAllCloudConfigs();
    }

    /**
     * Set google config
     *
     * @param array $data Data to set config
     *
     * @return void
     */
    public function setParams($data)
    {
        WpmfAddonHelper::saveCloudConfigs($data);
    }

    /**
     * Redirect url
     *
     * @param string $location URL
     *
     * @return void
     */
    public function redirect($location)
    {
        if (!headers_sent()) {
            header('Location: ' . $location, true, 303);
        } else {
            // phpcs:ignore WordPress.Security.EscapeOutput -- Content already escaped in the method
            echo "<script>document.location.href='" . str_replace("'", '&apos;', $location) . "';</script>\n";
        }
    }

    /**
     * Logout google drive app
     *
     * @return void
     */
    public function ggLogout()
    {
        $google = new WpmfAddonGoogleDrive();
        $google->logout();
        $data                      = $this->getParams();
        $data['googleBaseFolder']  = '';
        $data['googleCredentials'] = '';
        $this->setParams($data);
        $this->redirect(admin_url('options-general.php?page=option-folder#cloud'));
    }
}
