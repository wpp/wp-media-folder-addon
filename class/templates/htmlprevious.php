<div class="wpmfaddonfile entry folder parentfolder" data-id="<?php echo esc_html($parent) ?>" data-name="Drive của tôi">
    <div class="entry_block">
        <div class="entry_thumbnail">
            <div class="entry_thumbnail-view-bottom">
                <div class="entry_thumbnail-view-center">
                    <a class="entry_link">
                        <?php
                        // phpcs:ignore WordPress.Security.EscapeOutput -- Content already escaped in the method
                        echo $thumbnail_pre;
                        ?>
                    </a>
                </div>
            </div>
        </div>
        <div class="entry_name">
            <a class="entry_link">
                <div class="entry-name-view">
                    <span><strong><?php esc_html_e('Previous folder', 'wpmfAddon') ?></strong></span>
                </div>
            </a>
        </div>
    </div>
</div>