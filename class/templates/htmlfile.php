<?php
if (!isset($checktype)) {
    $checktype = '';
}
?>
<div class="wpmfaddonfile entry file <?php echo esc_html($checktype) ?>" data-id="<?php echo esc_html($id) ?>"
     data-name="<?php echo esc_html($name) ?>">
    <div class="entry_block">
        <div class="entry_edit">
            <i data-id="<?php echo esc_html($id) ?>" class="wpmfaddonicon-edit zmdi zmdi-edit"></i>
            <i data-id="<?php echo esc_html($id) ?>" class="wpmfaddonicon-delete zmdi zmdi-delete"></i>

            <a href="<?php echo esc_html($downloadlink) ?>" class="wpmfdocfile"
               title="<?php esc_html_e('Download file', 'wpmfAddon') ?>"><i
                        data-id="<?php echo esc_html($id) ?>" class="wpmfaddonicon-download zmdi zmdi-cloud-download"></i></a>

            <?php if (!empty($display_preview)) : ?>
                <a class="wpmflinkfile <?php echo esc_html($type) ?>" title="<?php echo esc_html($name) ?>"
                   data-link="<?php echo esc_html($downloadlink) ?>" data-id="<?php echo esc_html($id) ?>"
                   href="<?php echo esc_html($lightboxlink) ?>">
                    <i title="<?php esc_html_e('Preview file', 'wpmfAddon') ?>"
                       class="wpmfaddonicon-preview zmdi zmdi-eye"></i>
                </a>
            <?php endif; ?>
        </div>
        <div class="entry_checkfile">
            <input title class="wpmf_checkfile" type="checkbox" data-type="<?php echo esc_html($mimeType) ?>" value="<?php echo esc_html($id) ?>">
        </div>
        <a class="entry_link" title="<?php echo esc_html($name) ?>" data-filename="<?php echo esc_html($name) ?>">
            <div class="entry_thumbnail">
                <div class="entry_thumbnail-view-bottom">
                    <div class="entry_thumbnail-view-center">
                        <a href="#">
                            <img class="imgthumb imgloading" data-id="<?php echo esc_html($id) ?>"
                                 src="<?php echo esc_html(WPMFAD_PLUGIN_URL) . '/assets/images/Loading_icon.gif' ?>">
                            <img class="imgthumb imgfile" data-id="<?php echo esc_html($id) ?>" src="<?php echo esc_html($thumbnail) ?>">
                        </a>
                    </div>
                </div>
            </div>
            <div class="entry_name">
                <div class="entry-name-view">
                    <span class="wpmf_filename" data-ext="<?php echo esc_html($infofile['extension']) ?>"
                          data-name="<?php echo esc_html($infofile['filename']) ?>"><?php echo esc_html($name) ?></span>
                </div>
            </div>
        </a>
    </div>
</div>