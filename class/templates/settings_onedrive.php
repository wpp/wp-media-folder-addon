<div class="content-wpmf-onedrive">
    <?php
    // phpcs:ignore WordPress.Security.NonceVerification.NoNonceVerification -- No action, nonce is not required
    $page     = isset($_GET['page']) ? '?page=' . $_GET['page'] : '';
    $location = get_admin_url(null, 'admin.php' . $page);

    $appInfo = $onedriveDrive->getClient();
    if (is_wp_error($appInfo)) {
        echo '<div id="message" class="error"><p>' . esc_html($appInfo->get_error_message()) . '</p></div>';
        return false;
    }

    ?>

    <div>
        <h4><?php esc_html_e('OneDrive Client ID', 'wpmfAddon') ?></h4>
        <div>
            <input title name="OneDriveClientId" type="text" class="regular-text wpmf_width_100 p-lr-20"
                   value="<?php echo esc_attr($onedriveconfig['OneDriveClientId']) ?>">
            <p class="description" id="tagline-description">
                <?php esc_html_e('Insert your OneDrive Application Id here.
                     You can find this Id in the OneDrive dev center', 'wpmfAddon') ?>
            </p>
        </div>
    </div>

    <div class="m-t-60">
        <h4><?php esc_html_e('OneDrive Client Secret', 'wpmfAddon') ?></h4>
        <div>
            <input title name="OneDriveClientSecret" type="text" class="regular-text wpmf_width_100 p-lr-20"
                   value="<?php echo esc_attr($onedriveconfig['OneDriveClientSecret']) ?>">
            <p class="description" id="tagline-description">
                <?php esc_html_e('Insert your OneDrive Secret here.
                     You can find this secret in the OneDrive dev center', 'wpmfAddon') ?>
            </p>
        </div>
    </div>

    <div class="m-t-60">
        <h4><?php esc_html_e('Redirect URIs', 'wpmfAddon') ?></h4>
        <div>
            <input title name="redirect_uris" type="text" id="home" readonly
                   value="<?php echo esc_attr(admin_url()); ?>"
                   class="regular-text wpmf_width_100 p-lr-20 code">
        </div>
    </div>

    <a target="_blank" class="m-t-50 ju-button no-background orange-button waves-effect waves-light"
       href="https://www.joomunited.com/documentation/wp-media-folder-cloud-addon#toc-iv-onedrive-integration">
        <?php esc_html_e('Read the online documentation', 'wpmfAddon') ?>
    </a>
</div>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('.wpmf_onedrive_logout').click(function () {
            $.ajax({
                url: ajaxurl,
                method: 'POST',
                dataType: 'json',
                data: {
                    action: 'wpmf_onedrive_logout'
                },
                success: function (response) {
                    location.reload(true)
                }
            });
        });
    });
</script>