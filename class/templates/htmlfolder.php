<div class="wpmfaddonfile entry folder" data-id="<?php echo esc_html($id) ?>" data-name="<?php echo esc_html($name) ?>">
    <div class="entry_block">
        <div class="entry_edit">
            <i data-id="<?php echo esc_html($id) ?>" class="wpmfaddonicon-edit zmdi zmdi-edit"></i>
            <i data-id="<?php echo esc_html($id) ?>" class="wpmfaddonicon-delete zmdi zmdi-delete"></i>
        </div>
        <div class="entry_thumbnail">
            <div class="entry_thumbnail-view-bottom">
                <div class="entry_thumbnail-view-center">
                    <a class="entry_link">
                        <?php
                        // phpcs:ignore WordPress.Security.EscapeOutput -- Content already escaped in the method
                        echo $thumbnail;
                        ?>
                    </a>
                </div>
            </div>
        </div>
        <div class="entry_name">
            <a class="entry_link">
                <div class="entry-name-view">
                    <span class="wpmf_filename" data-ext="<?php echo esc_html($extension) ?>"
                          data-name="<?php echo esc_html($infofile['filename']) ?>"><?php echo esc_html($name) ?></span>
                </div>
            </a>
        </div>
    </div>
</div>