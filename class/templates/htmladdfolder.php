<div class="entry folder newfolder" data-parent="<?php echo esc_html($parent); ?>">
    <div class="entry_block">
        <div class="entry_thumbnail">
            <div class="entry_thumbnail-view-bottom">
                <div class="entry_thumbnail-view-center">
                    <a class="entry_link">
                        <?php
                        // phpcs:ignore WordPress.Security.EscapeOutput -- Content already escaped in the method
                        echo $thumbnail_newfolder;
                        ?>
                    </a>
                </div>
            </div>
        </div>
        <div class="entry_name">
            <a class="entry_link">
                <div class="entry-name-view">
                    <span><?php esc_html_e('Add folder', 'wpmfAddon') ?></span>
                </div>
            </a>
        </div>
    </div>
</div>