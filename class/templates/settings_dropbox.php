<div class="content-wpmf-dropbox">
    <div>
        <h4><?php esc_html_e('App Key', 'wpmfAddon') ?></h4>
        <div>
            <input title name="dropboxKey" type="text" class="regular-text wpmf_width_100 p-lr-20"
                   value="<?php echo esc_attr($dropboxconfig['dropboxKey']) ?>">
        </div>
    </div>

    <div class="m-t-60">
        <h4><?php esc_html_e('App Secret', 'wpmfAddon') ?></h4>
        <div>
            <input title name="dropboxSecret" type="text" class="regular-text wpmf_width_100 p-lr-20"
                   value="<?php echo esc_attr($dropboxconfig['dropboxSecret']) ?>">
        </div>
    </div>

    <?php if ($Dropbox->checkAuth()) { ?>
        <div class="m-t-60">
            <h4><?php esc_html_e('Authorization Code', 'wpmfAddon') ?></h4>
            <div>
                <input title name="dropboxAuthor" type="text" value="" class="regular-text wpmf_width_100 p-lr-20">
            </div>
        </div>
    <?php } else { ?>
        <div  class="m-t-60" style="display: none">
            <h4><?php esc_html_e('Authorization Code', 'wpmfAddon') ?></h4>
            <div>
                <input title name="dropboxAuthor" type="text" value="" class="regular-text wpmf_width_100 p-lr-20">
            </div>
        </div>
    <?php } ?>

    <a target="_blank" class="m-t-50 ju-button no-background orange-button waves-effect waves-light"
       href="https://www.joomunited.com/documentation/wp-media-folder-cloud-addon#toc-iii-dropbox-integration">
        <?php esc_html_e('Read the online documentation', 'wpmfAddon') ?>
    </a>
</div>