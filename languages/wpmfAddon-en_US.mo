��    ]            �      �  '   �  
     
         +  
   3     >  "   Q  #   t     �     �     �     �  "   �                     +     9     R     Z  	   _     i  '   o     �     �     �     �     �               -     ?     U     r     �     �     �     �     �     �     	     2	     9	     V	     t	     }	     �	     �	     �	     �	     �	     �	     
     '
     ;
      J
     k
     q
     
  
   �
     �
     �
     �
     �
     �
       *   #  %   N     t     �     �     �     �     �     �     �  
   �  "   �  )        :     K     ]  @   e  F   �  -   �          3     P  	   i     s     �     �    �  '   �  
   �  
   �       
          "   +  #   N     r     y     �     �  "   �     �     �     �               ,     4  	   9     C  '   I     q     �     �     �     �     �     �               /     L     a     r     �     �     �     �     �               0     N     W     k          �     �     �     �     �                $     E     K     Y  
   t          �     �     �     �     �  *   �  %   (     N     [     k     �     �     �     �     �  
   �  "   �  )   �          %     7  @   ?  F   �  -   �     �          *  	   C     M     a     t   A PHP extension stopped the file upload Add Folder Add folder App Key App Secret Authorization Code Browse and upload files to Dropbox Browse and upload files to OneDrive Cancel Change Filename Configure now Could not start authorization:  Couldn't connect to OneDrive API:  Delete Delete Folder Delete selected files Download file Drag your files here ... Dropbox Edit Embed PDF Error Error communicating with OneDrive API:  Failed to add folder Failed to delete item Failed to move entry Failed to rename entry Failed to write file to disk File is too big File is too large File is too small File type not allowed Files imported with success! Filetype not allowed Google Client ID Google Client Secret Google Drive Image exceeds maximum height Image exceeds maximum width Image requires a minimum height Image requires a minimum width Import Import file in media library Import files in media library In queue Insert Dropbox File Insert Google Drive Insert OneDrive File Insert file in content Insert files in content JavaScript origins Load Dropbox media Load Google Drive media Load OneDrive media Max file size: Maximum number of files exceeded Media Media Library Missing a temporary folder New Folder No file was uploaded Not uploaded to Google Drive Not uploaded to OneDrive OneDrive OneDrive Client ID OneDrive Client Secret Oops! This shouldn\'t happen... Try again! Please give a name to this new folder Preview file Previous folder Read the online documentation Redirect URIs Refresh Remove Save Search for files Select all Share-one-Drive isn't ready to run Something went wrong... See settings page Sort (Ascending) Sort (Descending) Success The uploaded file exceeds the post_max_size directive in php.ini The uploaded file exceeds the upload_max_filesize directive in php.ini The uploaded file was only partially uploaded Upload files to Dropbox Upload files to Google Drive Upload files to OneDrive Uploading Uploading to Server View documentation You are here  :  Project-Id-Version: 
Report-Msgid-Bugs-To: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2018-08-29T07:12:25+00:00
PO-Revision-Date: 2018-08-29T07:12:25+00:00
Language: 
Version: 2.1.6
 A PHP extension stopped the file upload Add Folder Add folder App Key App Secret Authorization Code Browse and upload files to Dropbox Browse and upload files to OneDrive Cancel Change Filename Configure now Could not start authorization:  Couldn't connect to OneDrive API:  Delete Delete Folder Delete selected files Download file Drag your files here ... Dropbox Edit Embed PDF Error Error communicating with OneDrive API:  Failed to add folder Failed to delete item Failed to move entry Failed to rename entry Failed to write file to disk File is too big File is too large File is too small File type not allowed Files imported with success! Filetype not allowed Google Client ID Google Client Secret Google Drive Image exceeds maximum height Image exceeds maximum width Image requires a minimum height Image requires a minimum width Import Import file in media library Import files in media library In queue Insert Dropbox File Insert Google Drive Insert OneDrive File Insert file in content Insert files in content JavaScript origins Load Dropbox media Load Google Drive media Load OneDrive media Max file size: Maximum number of files exceeded Media Media Library Missing a temporary folder New Folder No file was uploaded Not uploaded to Google Drive Not uploaded to OneDrive OneDrive OneDrive Client ID OneDrive Client Secret Oops! This shouldn\'t happen... Try again! Please give a name to this new folder Preview file Previous folder Read the online documentation Redirect URIs Refresh Remove Save Search for files Select all Share-one-Drive isn't ready to run Something went wrong... See settings page Sort (Ascending) Sort (Descending) Success The uploaded file exceeds the post_max_size directive in php.ini The uploaded file exceeds the upload_max_filesize directive in php.ini The uploaded file was only partially uploaded Upload files to Dropbox Upload files to Google Drive Upload files to OneDrive Uploading Uploading to Server View documentation You are here  :  